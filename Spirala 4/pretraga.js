let Pretraga = (function () {
  
  let predmeti;

  function postaviElemente(postavljeniPredmeti) 
  {
    predmeti = postavljeniPredmeti;
    return predmeti;
  }

  function pretragaPredmet(predmet) {
         
    var redovi = document.getElementsByClassName("tabela1")[0].getElementsByTagName("tr");    
    for (let i = 0; i < redovi.length; i++) 
      if (redovi[i].getElementsByTagName("td")[0]) 
      {
        var tekst = redovi[i].getElementsByTagName("td")[0].textContent || redovi[i].getElementsByTagName("td")[0].innerText;
        if (tekst.toUpperCase().indexOf(predmet.toUpperCase()) > -1) 
          redovi[i].style.display = "";
        else 
          redovi[i].style.display = "none";
    }
  }

function pretragaNastavnik(nastavnik) {
         
  var redovi = document.getElementsByClassName("tabela1")[0].getElementsByTagName("tr");    
  for (let i = 0; i < redovi.length; i++) 
    if (redovi[i].getElementsByTagName("td")[0]) 
    {
      var tekst = redovi[i].getElementsByTagName("td")[2].textContent || redovi[i].getElementsByTagName("td")[2].innerText;
      if (tekst.toUpperCase().indexOf(nastavnik.toUpperCase()) > -1) 
        redovi[i].style.display = "";
      else 
        redovi[i].style.display = "none";
    }
  }

  function pretragaGodina(godina) {
      
    var redovi = document.getElementsByClassName("tabela1")[0].getElementsByTagName("tr");  
    var i;
    if (godina == "1")
    {
      for (i = 0; i < redovi.length; i++)
        redovi[i].style.display = "";
      for (i = 12; i < redovi.length; i++)    
        redovi[i].style.display = "none";
    }
    else if (godina == 2)
    {
      for (i = 0; i < 14; i++)
        redovi[i].style.display = "none";
      for (i = 12; i < redovi.length; i++)    
        redovi[i].style.display = "";
    }
    else
    {
      for (i = 0; i < redovi.length; i++)    
        redovi[i].style.display = "";
    }
  }

  return {
      pretragaGodina: pretragaGodina,
      postaviElemente: postaviElemente,
      pretragaPredmet: pretragaPredmet,
      pretragaNastavnik: pretragaNastavnik      
  }
}());