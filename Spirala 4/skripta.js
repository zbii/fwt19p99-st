
  //Imamo dvije globalne varijable ciji je zadatak da cuvaju pocetni 
  //izgled prozora, tj. elemente na stranici, kako bi prelazak
  //sa sortiranja na listu predmeta bio responsive
  //Varijabla "izgled" se koristi pri restartovanju prozora nakon sortiranja
  //kako bi se predmeti pri kliku na dugmad SviPredmeti, PrvaGodina, DrugaGodina
  //vratili na početne pozicije.
  //Druga varijabla služi da se spasi trenutni izgled prozora, također
  //važno pri kliku više buttona uzastopno

var izgled;
var pamtiIzgled;

window.onload = function () 
{    
  //U ovoj funkciji postavljamo vrijednosti globalnih varijabli
  //i to samo pod uslovom da element sa IDem "predmeti" postoji
  //s obzirom da je skripta povezana sa tri različita .html filea

  if (document.getElementById("predmeti"))
  {
    izgled = document.getElementById("predmeti").innerHTML;
    pamtiIzgled = document.getElementsByClassName('predmet');  
  } 
}

/*******************************************************************
 *           Funkcije za html stranicu predmeti.html               *
 *******************************************************************/

function restartuj()
{
  //Vraćamo stranici početni izgled
  document.getElementById('predmeti').innerHTML = izgled;
}

function sakrijprvu() 
{
    //Restartujemo stranicu u slučaju da je došlo do sortiranja 
    //Prije pritiska na ovo dugme
    restartuj();

    //Dohvaćamo predmete prve i predmete druge godine
    //i određujemo koji će se priazati a koji ne
    var x = document.getElementById('pgs');
    var y = document.getElementById('dgs');
    x.style.display = "none"; 
    y.style.display = "block";

    //Pamtimo izgled prozora u slučaju da se poslije ove funkcije
    //pozove funkcija sortiranja
    pamtiIzgled = y.getElementsByClassName('predmet');   
}

function sakrijdrugu() 
{
  //Restartujemo stranicu u slučaju da je došlo do sortiranja 
  //Prije pritiska na ovo dugme
  restartuj();

  //Dohvaćamo predmete prve i predmete druge godine
  //i određujemo koji će se priazati a koji ne
  var x = document.getElementById('pgs');
  var y = document.getElementById('dgs');
  y.style.display = "none"; 
  x.style.display = "block";

  //Pamtimo izgled prozora u slučaju da se poslije ove funkcije
  //pozove funkcija sortiranja
  pamtiIzgled = x.getElementsByClassName('predmet');   
}

function prikazisve()
{
  //Restartujemo stranicu u slučaju da je došlo do sortiranja 
  //Prije pritiska na ovo dugme
  restartuj();

  //Dohvaćamo predmete prve i predmete druge godine
  //i određujemo koji će se priazati a koji ne
  var x = document.getElementById('pgs');
  var y = document.getElementById('dgs');
  x.style.display = "block";
  y.style.display = "block";

  //Pamtimo izgled prozora u slučaju da se poslije ove funkcije
  //pozove funkcija sortiranja
  pamtiIzgled = document.getElementsByClassName('predmet');
}

function sortiraj()
{
  //Kreiramo 2 prazna niza u koje umecemo dohvacene podatke
  var niz = [];
  var imena = [];

  //Umetanje u dva niza (jedan pamti cijeli div predmeta, a drugi samo ime)
  for (var i = pamtiIzgled.length >>> 0; i--;) { 
    niz[i] = pamtiIzgled[i];
    imena[i] = pamtiIzgled[i].getElementsByClassName("imepredmeta")[0].innerText;  
  }

  //Sortiranje - dummy bubble sort
  for (i = 0; i < niz.length; i++) 
  { 
    flag = false; 
    for (j = 0; j < niz.length - i; j++) 
    { 
      //Provjeravamo ime predmeta i na osnovu toga swapamo i predmet i div cijelog predmeta
      if (imena[j] > imena[j+1]) 
      { 
        var temp1 = niz[j];
        niz[j] = niz[j+1];
        niz[j+1] = temp1;

        var temp2 = imena[j];
        imena[j] = imena[j+1];
        imena[j+1] = temp2;
             
        flag = true; 
    } 
   }   
   // Ako se nije desila promjena u ovoj, breakaj
   if (flag == false) 
      break; 
  }
  
  //Dohvacamo div u kome se nalaze predmeti i ocistimo ga 
  var svipredmeti = document.getElementById('predmeti');
  svipredmeti.innerHTML = "";

  //Umecemo predmete po sortiranom rasporedu
  for(var i = 0, l = niz.length; i < l; i++) {
    svipredmeti.appendChild(niz[i]);
  }
}

/********************************************************************/

/*******************************************************************
 *            Funkcije za html stranicu godine.html                *
 *******************************************************************/

  function oboji(a)  {

    //Uzimamo prvu/lijevu tabelu
    var table = document.getElementsByClassName("tabela1")[0];

    //Prolazimo kroz redove tabele i tražimo podudaranje treće kolone
    //sa proslijeđenim parametrom (koji sadrži info na kojeg smo profesora
    //hoverali), kada nađemo onda obojimo pojedinačne ćelije tog reda
    //u žutu boju
    for (var x = 0, y = table.rows.length; x < y; x++) 
        for (var z = 0, r = table.rows[x].cells.length; z < r; z++) 
          if(table.rows[x].cells[z].innerHTML == a)
          {
            table.rows[x].cells[0].style.backgroundColor = "yellow";
            table.rows[x].cells[1].style.backgroundColor = "yellow";
            table.rows[x].cells[2].style.backgroundColor = "yellow";
          } 
  }

  function obrisiboju(a)  {

    //Uzimamo prvu/lijevu tabelu
    var table = document.getElementsByClassName("tabela1")[0];

    //Prolazimo kroz redove tabele i tražimo podudaranje treće kolone
    //sa proslijeđenim parametrom (koji sadrži info na kojeg smo profesora
    //hoverali), kada nađemo onda obojimo pojedinačne ćelije tog reda
    //nazad u početnu boju
    for (var x = 0, y = table.rows.length; x < y; x++) 
        for (var z = 0, r = table.rows[x].cells.length; z < r; z++) 
          if(table.rows[x].cells[z].innerHTML == a)
          {
            table.rows[x].cells[0].style.backgroundColor = "#202020";
            table.rows[x].cells[1].style.backgroundColor = "#202020";
            table.rows[x].cells[2].style.backgroundColor = "#202020";
          } 
  }

  /******************************************************************/

/*******************************************************************
 *            Funkcije za html stranicu osoblje.html               *
 *******************************************************************/

 //funkcija prihvata dva parametra, prvi je pokazivač iz kojeg dobivamo
 //koju sliku treba uvećati, a drugi je bool preko kojeg dobivamo
 //info da li je riječ o profesoru ili asistentu
 //Nakon toga proširujemo sliku na veličinu elementa
 function uvecaj(x, ko) {

    if (ko)
      var pos1 = document.getElementsByClassName("profesoriel")[0];    
    else 
      var pos1 = document.getElementsByClassName("asistentiel")[0];  

    var visina = pos1.offsetHeight;
    var duzina = pos1.offsetWidth;
     
          x.style.height= visina + 'px'; 
          x.style.width= duzina + 'px';         
}

//Vraćamo početnu dimenziju slike koristeći funkciju naturalHeight
function smanji(r)  
{
  r.style.height = r.naturalHeight + "px";
  r.style.width = r.naturalWidth + "px";
}

  /******************************************************************/