let assert = chai.assert;
describe('Pretraga', function() {
 describe('pretragaPredmet()', function() {
   it('Provjera da li su prikazani svi predmeti kada je unesen prazan string u metodu pretragaPredmet', function() {
     
    Pretraga.pretragaPredmet("");
    let redovi = document.getElementsByClassName("tabela1")[0].getElementsByTagName("tr");
    let predmeti = 0;
    let i = 0;
    
    for(i = 0; i < redovi.length; i++) 
        if(redovi[i] && redovi[i].style.display ===  "" && redovi[i].cells[0].tagName === "TD") 
            predmeti++;   

    assert.equal(predmeti, 18, 'Niz predmeta treba imati 18 predmeta');
   });

   it('Provjera da nije prikazan niti jedan predmet kada se proslijedi imeNastavnika koje ne postoji niti za jedan predmet', function() {
     
    Pretraga.pretragaNastavnik("Huse Fatkic"); // Thank God
    let redovi = document.getElementsByClassName("tabela1")[0].getElementsByTagName("tr");
    let predmeti = 0;
    let i = 0;

    for(i = 0; i < redovi.length; i++) 
        if(redovi[i] && redovi[i].style.display ===  "" && redovi[i].cells[0].tagName === "TD") 
            predmeti++;          

    assert.equal(predmeti, 0, 'Niz predmeta ne treba sadržavati nijedan predmet profesora Huse Fatkića');

   });


   it('Provjera da se prikazuju samo predmeti nekog profesora kada se proslijedi cijelo njegovo ime i prezime', function() {
     
    Pretraga.pretragaNastavnik("Željko Jurić"); 
    let redovi = document.getElementsByClassName("tabela1")[0].getElementsByTagName("tr");
    let predmeti = 0;
    let i = 0;

    for(i = 0; i < redovi.length; i++) {
        if(redovi[i] && redovi[i].style.display ===  "" && redovi[i].cells[0].tagName === "TD") {
            predmeti++;
        }
    }

    assert.equal(predmeti, 2, 'Niz predmeta treba sadržavati ukupno 2 predmeta, onoliko koliko predaje prof. Jurić');

   });

   it('Provjera da su sakriveni svi predmeti koji nisu na drugoj godini kada se pozove pretragaGodina', function() {
     
    Pretraga.pretragaGodina("2"); 
    let redovi = document.getElementsByClassName("tabela1")[0].getElementsByTagName("tr");
    let prikazanPogresan = false;
    let i = 0;

    //Ako nađemo na neki red sa class-nameom prva godina, postavi flag na true
    for(i = 0; i < redovi.length; i++) {
        if(redovi[i] && redovi[i].style.display ===  "" && redovi[i].className === "prvagodina") {
            prikazanPogresan = true;            
        }
    }
    
    assert.equal(prikazanPogresan, false, 'Svi predmeti prve godine moraju biti sakriveni kada se unese 2');

   });

   it('Provjera da su sakriveni svi predmeti koji nisu na prvoj godini kada se pozove pretragaGodina', function() {
     
    Pretraga.pretragaGodina("1"); 
    let redovi = document.getElementsByClassName("tabela1")[0].getElementsByTagName("tr");
    let prikazanPogresan = false;
    let i = 0;

    //Ako nađemo na neki red sa class-nameom prva godina, postavi flag na true
    for(i = 0; i < redovi.length; i++) {
        if(redovi[i] && redovi[i].style.display ===  "" && redovi[i].className === "drugagodina") {
            prikazanPogresan = true;            
        }
    }

    assert.equal(prikazanPogresan, false, 'Svi predmeti druge godine moraju biti sakriveni kada se unese 1');

   });

   it('Provjera da li se prikazuju svi predmeti kada se pretragaGodina pozove sa nekom drugom vrijednošću', function() {
     
    Pretraga.pretragaGodina("ABC"); 
    let redovi = document.getElementsByClassName("tabela1")[0].getElementsByTagName("tr");
    let predmeti = 0;
    let i = 0;

    for(i = 0; i < redovi.length; i++) {
        if(redovi[i] && redovi[i].style.display ===  "" && redovi[i].cells[0].tagName === "TD") {
          predmeti++;            
        }
    }
    
    assert.equal(predmeti, 18, 'Svi predmeti se moraju prikazati kada se unese vrijednost drugačija od 1 i 2');

   });
   
   it('Provjera da li se prikazuju svi predmeti kada se pretragaPredmet pozove sa imenom koje ne postoji', function() {
     
    Pretraga.pretragaPredmet("Osnove računarstva"); 
    let redovi = document.getElementsByClassName("tabela1")[0].getElementsByTagName("tr");
    let predmeti = 0;
    let i = 0;

    for(i = 0; i < redovi.length; i++) {
        if(redovi[i] && redovi[i].style.display ===  "" && redovi[i].cells[0].tagName === "TD") {
          predmeti++;            
        }
    }
    
    assert.equal(predmeti, 0, 'Nijedan predmet ne treba biti prikazan');

   });

   it('Provjera da li se prikazuju svi predmeti kada se pretragaNastavnik pozove sa nekom paznim stringom', function() {
     
    Pretraga.pretragaNastavnik(""); 
    let redovi = document.getElementsByClassName("tabela1")[0].getElementsByTagName("tr");
    let predmeti = 0;
    let i = 0;

    for(i = 0; i < redovi.length; i++) {
        if(redovi[i] && redovi[i].style.display ===  "" && redovi[i].cells[0].tagName === "TD") {
          predmeti++;            
        }
    }
    
    assert.equal(predmeti, 18, 'Svi predmeti se moraju prikazati kada se unese prazan string za nastavnika');

   });





 });
});